package com.example.stack;

public class Main {
    public static void main(String[] args) {
           try {
               // 1. Generic stack test cases
               System.out.println("\n");
               GenericStack<Integer> myStack = new GenericStack<>(5);
               for (int i = 0; i < 5; i++) {
                   myStack.push(i);
               }
               // Throws error since we exceeded stack size.
               // myStack.push(20);
               myStack.printStackElements();
               int poppedElement = myStack.pop();
               System.out.println("");
               System.out.println("Popped element is: " +poppedElement);
               System.out.print("Stack after removing an element: ");
               myStack.printStackElements();
               System.out.println("");
                int topElement = myStack.peek();
                System.out.println("Top element of stack is: " +topElement);

                // 2. Reversing string (Please refer ReverseStrings.java)
               System.out.println("\n");
                ReverseStrings reverseStringsClass = new ReverseStrings("Data Structures are cool");
                String reversedString = reverseStringsClass.getReversedString();
                System.out.println("Reversed string is: " + reversedString);

                // 3. Parentheses checking (Please refer CheckParentheses.java)
               System.out.println("\n");
               CheckParentheses firstCase = new CheckParentheses("((((( This string is valid )))))");
                firstCase.validate();

               CheckParentheses secondCase = new CheckParentheses("((((( This string has too many opening parentheses ))");
               secondCase.validate();

               CheckParentheses thirdCase = new CheckParentheses("((((( This string has too many closing parentheses ))))))))");
               thirdCase.validate();

           }
           catch (Exception e) {
               System.out.println(e.getMessage());
           }
    }
}
