package com.example.stack;

public class ReverseStrings {
    private String stringToReverse;

    public ReverseStrings(String stringToReverse) {
        this.stringToReverse = stringToReverse;
    }

    public String getReversedString() throws Exception {
        String reversedString = "";
        int lengthOfString = this.stringToReverse.length();
        GenericStack<Character> myStack = new GenericStack<>(lengthOfString);
        for (int i = 0; i < lengthOfString; i++) {
            myStack.push(this.stringToReverse.charAt(i));
        }

        for (int i = 0; i < lengthOfString; i++) {
            reversedString += myStack.pop();
        }
        return  reversedString;
    }
}
