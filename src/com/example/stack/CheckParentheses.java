package com.example.stack;

public class CheckParentheses {
    private String stringToValidate;

    public CheckParentheses(String stringToValidate) {
        this.stringToValidate = stringToValidate;
    }

    public void validate() throws Exception {
        int lengthOfString = this.stringToValidate.length();
        GenericStack<Character> stack = new GenericStack<>(lengthOfString);
        for (int i = 0; i < lengthOfString; i++) {
            // Add item to stack if opening parentheses are found
            if (this.stringToValidate.charAt(i) == '(') {
                stack.push(this.stringToValidate.charAt(i));
            } else if (this.stringToValidate.charAt(i) == ')') {
                // If we found closing parentheses check if stack is empty.
                // If it's empty we have many closing parentheses
                if (stack.getCurrentStackSize() == 0) {
                    System.out.println("The string '"+this.stringToValidate+"' has too many closing parentheses");
                    return;
                }
                // Pop the item since we have encounter valid pair of parentheses
                stack.pop();
            }
        }
        // At last if stack is empty parentheses are balanced, else we have many opening parentheses
        if (stack.getCurrentStackSize() == 0) {
            System.out.println("The string '"+this.stringToValidate+"' has correct number of parentheses");
        } else {
            System.out.println("The string '"+this.stringToValidate+"' has too many opening parentheses");
        }
    }
}
