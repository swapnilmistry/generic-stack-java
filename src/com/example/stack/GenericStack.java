package com.example.stack;

public class GenericStack<T> {
    private T stack[];
    private int maxSize;
    private int currentStackSize;

    public GenericStack(int maxSize) {
        this.maxSize = maxSize;
        this.stack = (T[]) new Object[maxSize];
        this.currentStackSize = 0;
    }

    public boolean isEmpty() {
        if (this.currentStackSize == 0) {
            return true;
        }
        return false;
    }

    public void push(T item) throws Exception {
        if (this.currentStackSize < this.maxSize) {
            this.stack[this.currentStackSize] = item;
            this.currentStackSize += 1;
            return;
        }
        throw new Exception("Cannot add element. Stack size exceeded");
    }

    public T pop() throws Exception {
        if (this.currentStackSize <= 0) {
            throw new Exception("Cannot pop element as stack is already empty.");
        }
        int size = this.currentStackSize - 1;
        this.currentStackSize -= 1;
        return this.stack[size];
    }

    public void popAll() {
        this.currentStackSize = 0;
    }

    public T peek() {
        return this.stack[this.currentStackSize - 1];
    }

    public void printStackElements() {
        for (int i = 0; i < this.currentStackSize; i++) {
            System.out.print(this.stack[i] + "\t");
        }
    }

    public int getCurrentStackSize() {
        return this.currentStackSize;
    }

}
